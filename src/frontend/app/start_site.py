from flask import Flask
from config import Config
# from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object(Config)
# login = LoginManager(app)
# from app import routes


from flask import render_template, flash, request
from config import Config
# from app import app
from forms import TypeForm, LoginForm



@app.route('/')
@app.route('/homepage')
def homepage():
	user = {'username': 'artur'}
	return render_template('homepage.html', title="Home", user=user)


types_cnt = 0
types_dict = {}
types_data = list()
mods = {1:"single", 2:"double", 3:"long"}

@app.route('/type_form', methods=['GET', 'POST'])
def type_form():
	global types_cnt, types_dict, types_data, mods
	form = TypeForm()
	refresh_pressed = form.refresh.data
	add_pressed = form.submit.data
	save_pressed = form.save.data
	max_wait = form.max_wait_time.data

	if add_pressed:
		data = form.types.data
		form.save_type(types_dict, types_cnt)
		if data:
			types_data.append(int(data))
		types_cnt += 1
	elif save_pressed:
		with open("app/data.txt", "w") as conf:
			print(len(types_data) - 1, file=conf)
			print(max_wait, file=conf)
			print(*types_data[1:], sep='', file=conf)
		# print(types_data)
		flash('Your sequence:')
		for i in range(1, len(types_data)):
			flash('{}'.format(mods[types_data[i]]))
		refresh()
	elif refresh_pressed:
		refresh()

	return render_template('type_form.html',
			title="OIT-55 ver.1",
			form=form,
			types_cnt=types_cnt,
			types_dict=types_dict,
			types_data=types_data,
			mods=mods)

@app.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	submit_pressed = form.submit.data

	if submit_pressed:
		login = form.login.data
		with open('app/login.txt', "w") as out:
			print(login, file=out)
	return render_template('login.html', title = "OIT-55", form=form)



def refresh():
	global types_cnt, types_data, types_dict
	types_cnt = 1
	types_dict.clear()
	types_data = list('0') #a little memmory leak

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=5500)
