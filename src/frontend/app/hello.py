from flask import Flask, render_template

app = Flask(__name__)

# from app import app

@app.route('/')
@app.route('/index')
def index():
	user = {'username': 'artur'}
	return render_template('index.html', title='Home', user=user)

if __name__ == '__main__':
	app.run(debug=True)