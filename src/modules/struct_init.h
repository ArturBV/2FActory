#ifndef STRUCT_INIT_H
#define STRUCT_INIT_H

#include <stdio.h>
#include <stdlib.h>
struct packet {
    int n;
    int max_wait;
    int* arr;
};
struct packet* init_packet(FILE* inp);
void deinit_packet(struct packet* pac);
int cmp_packet(struct packet* pac1, struct packet* pac2);
int check(FILE* inpFront, FILE* inpServ);

#endif /*STRUCT_INIT_H*/
