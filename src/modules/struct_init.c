#include "struct_init.h" 
#include <stdio.h>
#include <string.h>


struct packet* init_packet(FILE* inp) {
    
    int n, max_wait;
    struct packet* pac = malloc(sizeof(struct packet));
    
    fscanf(inp, "%d%d", &n, &max_wait);
    int* arr = malloc(sizeof(int) * n);
    char temp;
	fscanf(inp, "%c", &temp); // get '\n'
    for (int i = 0; i < n; i++) {
        fscanf(inp, "%c", &temp);
        arr[i] = (int) temp - 48;
    }
    
    pac->n = n;
    pac->max_wait = max_wait;
    pac->arr = arr;
    
    return pac;
}


void deinit_packet(struct packet* pac) {
    free(pac->arr);
    free(pac);
}


int cmp_packet(struct packet* pac1, struct packet* pac2) {
    // Принимает на вход 2 пакета
    // Если они не равны друг другу, то возвращает 0 (false)
    // Если они равны друг другу, то возвращает 1 (true)
    if (pac1->n != pac2->n) {
        return 0;
    }
    for (int i = 0; i < pac1->n; i++) {
        if ((pac1->arr)[i] != (pac2->arr)[i]) {
            return 0;
        }
    }
    return 1;
}


int check(FILE* inpFront, FILE* inpServ) {
	
	int MAX_LEN = 255;
	
	struct packet* pacFront = init_packet(inpFront);
	char s[MAX_LEN - 1] = {0};
	fgets(s, MAX_LEN, inpServ);
	int len = strlen(s);
	len--;
	int* arr = malloc(sizeof(int) * len);
	
	for (int i = 0; i < len; i++) {
		arr[i] = (int) s[i] - 48;
	}
	
	struct packet* pacServ = malloc(sizeof(struct packet));
	pacServ->n = len;
	pacServ->arr = arr;
	
	int flag = -1;
	
	if (cmp_packet(pacFront, pacServ)) {
		flag = 1;
	}
	else {
		flag = 0;
	}
	
	deinit_packet(pacServ);
	deinit_packet(pacFront);
	
	return flag;

}


int main(void) {
    
	FILE* inpFront = fopen("../frontend/app/data.txt", "r");
	FILE* inpServ = fopen("user.txt", "r");
	FILE* ans = fopen("check.txt", "w");

	int res = check(inpFront, inpServ);
	fpintf(ans, "%d", res);

	fclose(ans);
	fclose(inpFront);
	fclose(inpServ);
	
	return 0;

}
