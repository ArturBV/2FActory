#include "../json.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

int mode;
int sinr;
char* button_id;
int button_reliable;
json_value* section;


static void process_value(json_value* value, int depth);

static void process_object(json_value* value, int depth)
{
        int length, x;
        if (value == NULL) {
                return;
        }

        length = value->u.object.length;
        for (x = 0; x < length; x++) {
                section = value->u.object.values[x].value;

                if (strcmp(value->u.object.values[x].name, "externalId") == 0){
                        button_id = section->u.string.ptr;
                } else if (strcmp(value->u.object.values[x].name, "reliableDataService") == 0){
                        if (strcmp(section->u.string.ptr, "false")){
                                button_reliable = 0;
                        }else {
                                button_reliable = 1;
                        }
                }

                process_value(section, depth+1);
        }
}

static void process_value(json_value* value, int depth)
{
        if (value == NULL) {
                return;
        }
        
        if (value->type == json_object){
                process_object(value, depth+1);
        }
}

int main(void)
{
        char* filename;
        FILE *fp;
        struct stat filestatus;
        int file_size;
        char* file_contents;
        json_char* json;
        json_value* value;

        filename = "test.json";
        if ( stat(filename, &filestatus) != 0) {
                fprintf(stderr, "File %s not found\n", filename);
                return 1;
        }
        file_size = filestatus.st_size;
        file_contents = (char*)malloc(filestatus.st_size);
        if ( file_contents == NULL) {
                fprintf(stderr, "Memory error: unable to allocate %d bytes\n", file_size);
                return 1;
        }

        fp = fopen(filename, "rt");
        if (fp == NULL) {
                fprintf(stderr, "Unable to open %s\n", filename);
                fclose(fp);
                free(file_contents);
                return 1;
        }
        if ( fread(file_contents, file_size, 1, fp) != 1 ) {
                fprintf(stderr, "Unable to read content of %s\n", filename);
                fclose(fp);
                free(file_contents);
                return 1;
        }
        fclose(fp);

        json = (json_char*)file_contents;

        value = json_parse(json,file_size);

        if (value == NULL) {
                fprintf(stderr, "Unable to parse data\n");
                free(file_contents);
                exit(1);
        }

        process_value(value, 0);

        printf("%s %d\n", button_id, button_reliable);

        json_value_free(value);
        free(file_contents);
        return 0;
}
